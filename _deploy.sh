#!/bin/bash

# Usage: ./_deploy [--deployment <deployment-name>] [project...]
# Note: Only projects included in the deployment can be deployed (even through command line arguments).

DEPLOYMENT_elbe="telegraf system account chat cloud homepage mastodon newsletter talk tickets vault wiki polls"
DEPLOYMENT_schrote="telegraf system mail monitoring"
DEPLOYMENT_randauer_baggerloch="telegraf"

export domain_ingress="ingress.$(hostname)"
if [ "$(hostname)" != "elbe.s.softwerke.md" ]; then export traefik_wildcard_enabled=false; fi

################################################################################

set -eu
B=$(printf '\033[1;36m')
R=$(printf '\033[0m')

# Use short system hostname or first argument, trim away invalid characters.
if [ $# -ge 2 ] && [ "$1" = "--deployment" ]; then DEPLOYMENT_HOSTNAME="$2"; shift 2
else DEPLOYMENT_HOSTNAME=$(hostname --short)
fi
DEPLOYMENT_HOSTNAME=$(echo "$DEPLOYMENT_HOSTNAME" | sed 's/\..*//' | tr '-' '_' | tr -dc '[:alnum:]_')
echo "${B}Using deployment hostname:${R} $DEPLOYMENT_HOSTNAME"

# Loop through projects
DEPLOYMENT_ROOT=$(dirname "$(realpath "$0")")
DEPLOYMENT_PROJECTS=$(eval 'echo "${DEPLOYMENT_'"$DEPLOYMENT_HOSTNAME"'}"')
if [ -z "$DEPLOYMENT_PROJECTS" ]; then echo "${B}No projects to deploy on this host.${R}"; exit 1; fi
for project in $DEPLOYMENT_PROJECTS; do
	if [ $# -ge 1 ] && [[ ! " $* " =~ " $project " ]]; then
		echo "${B}SKIPPING project:${R} $project (not in arguments)"
		continue
	fi

	# Deploy project with Docker Compose
	echo "${B}Deploying project:${R} $project"
	cd "$DEPLOYMENT_ROOT"; cd "$project";
	docker compose pull
	docker compose up -d --remove-orphans

done
