# LDAP Schema

We don't have our own PEN yet, so we're using one by the [Fachschaftsrat Elektrotechnik an der TU Dresden](http://oidref.com/1.3.6.1.4.1.41374).

Under their OID, we're using `1923134` as a namespace for our schema.

For the `associationMember` schema (for internal email addresses etc.), we're using `1923134.10` as a base, and then `1923134.10.0` for the object class itself and incrmental values of the last field for the attributes.
