[global_tags]


# Configuration for telegraf agent
[agent]
    interval = "30s"
    debug = false
    hostname = "schrote"
    round_interval = true
    flush_interval = "10s"
    flush_jitter = "0s"
    collection_jitter = "0s"
    metric_batch_size = 1000
    metric_buffer_limit = 10000
    quiet = false
    logfile = ""
    omit_hostname = false

###############################################################################
#                                  OUTPUTS                                    #
###############################################################################

[[outputs.influxdb_v2]]
    urls = ["https://monitoring-influx.softwerke.md"]
    token = "$INFLUX_TOKEN"
    organization = "Softwerke"
    bucket = "schrote"


###############################################################################
#                                  INPUTS                                     #
###############################################################################

[[inputs.cpu]]
    percpu = true
    totalcpu = true
    collect_cpu_time = false
    report_active = false
[[inputs.disk]]
    ignore_fs = ["tmpfs", "devtmpfs", "devfs"]
    interval = "5m"
[[inputs.diskio]]
[[inputs.mem]]
[[inputs.nstat]]
[[inputs.system]]
[[inputs.swap]]
[[inputs.netstat]]
[[inputs.processes]]
[[inputs.kernel]]

# Borgbackup
[[inputs.exec]]
    commands = [
        "/usr/share/monitoring-user-scripts/influx-last-backup-age.sh /hostfs/etc/borgmatic-last-successful-backup.txt"
    ]
    timeout = "5s"
    data_format = "influx"

# See https://gist.github.com/mjf/0e20b2d82700c967fb830eb220fb7149

[[inputs.file]]
    name_override = "pressure"
    files = ["/proc/pressure/cpu", "/proc/pressure/io", "/proc/pressure/memory"]
    data_format = "grok"
    grok_patterns = [
        "%{NOTSPACE:type:tag} avg10=%{NUMBER:avg10:float} avg60=%{NUMBER:avg60:float} avg300=%{NUMBER:avg300:float} total=%{NUMBER:total:int}"
    ]
    file_tag = "subject"

[[inputs.docker]]
    endpoint = "unix:///hostfs/var/run/docker.sock"
