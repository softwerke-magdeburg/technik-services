#!/bin/sh
#
# Write file with time of last successful backup.

set -eu

if [ $# -ne 1 ]; then
	echo "Usage: $0 <FILE>"
	exit 1
fi

STAT_FILE="$1"

DATE_TIME=$(borgmatic info --archive latest --json | jq --raw-output .[0].archives[0].end)
echo "Last successfull borg backup was finished at:" > "$STAT_FILE"
echo $DATE_TIME >> "$STAT_FILE"
echo $(date +%s -d $DATE_TIME) >> "$STAT_FILE"
