<?php
  // This domain will be used to form e-mail addresses of new users
  // Specify an array with 'host' => 'domain' values to support multiple hosts
  // Supported replacement variables:
  // %h - user's IMAP hostname
  // %n - http hostname ($_SERVER['SERVER_NAME'])
  // %d - domain (http hostname without the first part)
  // %z - IMAP domain (IMAP hostname without the first part)
  // For example %n = mail.domain.tld, %t = domain.tld
  $config['mail_domain'] = 'softwerke.md';

  // Message size limit. Note that SMTP server(s) may use a different value.
  // This limit is verified when user attaches files to a composed message.
  // Size in bytes (possible unit suffix: K, M, G)
  $config['max_message_size'] = '50M';

  // Name your service. This is displayed on the login screen and in the window title
  $config['product_name'] = 'Softwerke Webmail';