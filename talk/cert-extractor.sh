#!/bin/sh
set -eu

function _extract_certs_from_acme {
  input_file=/etc/ssl/private/traefik/default.json
  output_dir=/etc/ssl/private/murmur
  # shellcheck disable=SC2002
  key=$(cat "$input_file" | python -c "
import sys,json
acme = json.load(sys.stdin)
for key, value in acme.items():
    certs = value['Certificates']
    if certs is not None:
        for cert in certs:
            if 'domain' in cert and 'key' in cert:
                if 'main' in cert['domain'] and cert['domain']['main'] == '${1}' or 'sans' in cert['domain'] and '${1}' in cert['domain']['sans']:
                    print(cert['key'])
                    break
")

  # shellcheck disable=SC2002
  cert=$(cat "$input_file" | python -c "
import sys,json
acme = json.load(sys.stdin)
for key, value in acme.items():
    certs = value['Certificates']
    if certs is not None:
        for cert in certs:
            if 'domain' in cert and 'certificate' in cert:
                if 'main' in cert['domain'] and cert['domain']['main'] == '${1}' or 'sans' in cert['domain'] and '${1}' in cert['domain']['sans']:
                    print(cert['certificate'])
                    break
")

  if [ -n "${key}${cert}" ]
  then
    echo "${key}" | base64 -d >"$output_dir/magdeburg.jetzt.cer" || exit 1
    echo "${cert}" | base64 -d >"$output_dir/magdeburg.jetzt.key" || exit 1
    echo "Cert found in $input_file for ${1}, written to $output_dir."

    return 0
  else
    echo "No key/cert found in $input_file"
    return 1
  fi
}

_extract_certs_from_acme magdeburg.jetzt
