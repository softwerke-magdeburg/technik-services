#!/bin/bash
set -eu
#
# Script to remove rooms without local users from the homeserver
#

TEMPDIR=$(mktemp -d)

# API Token can be copied from Element, user must have Admin Privilegs 
TOKEN="" 
HOMESERVER="https://chat.magdeburg.jetzt"

# get the first 300 Rooms ordered by number of local users in ascending order
curl --header "Authorization: Bearer ${TOKEN}" -o "${TEMPDIR}"/roomlist.json -fsS  "${HOMESERVER}/_synapse/admin/v1/rooms?limit=300&dir=b&order_by=joined_local_members"

# fitler for rooms without local users
ROOMS_TO_REMOVE=$(jq '[ .rooms[] | select(.joined_local_members == 0) ]' < "${TEMPDIR}"/roomlist.json)
ROOMS_TO_REMOVE_LENGTH=$( echo $ROOMS_TO_REMOVE | jq '. | length' )

echo "There are ${ROOMS_TO_REMOVE_LENGTH} rooms without local users."

for (( i=0; i<$ROOMS_TO_REMOVE_LENGTH; i++ )) ; do
	ROOM=$(echo $ROOMS_TO_REMOVE | jq ".[$i]")
	echo "Next room to be deleted is:"
	echo $ROOM | jq
	read -p "Are you sure? " -n 1 -r
	echo # (optional) move to a new line
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		echo "-- deleting --"
		ROOM_ID=$(echo $ROOM | jq '.room_id' -r)
		echo $ROOM_ID
		curl --header "Authorization: Bearer ${TOKEN}" -X DELETE -H "Content-Type: application/json" -d "{\"purge\": true}" -fsS "${HOMESERVER}/_synapse/admin/v1/rooms/${ROOM_ID}"
		echo #move to new line
	else
		echo "-- skipping --"
	fi
done

rm -rf "${TEMPDIR}"
