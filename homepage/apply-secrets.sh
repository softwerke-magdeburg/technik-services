#!/bin/sh
set -e

printf '<?php namespace ProcessWire;\n' > /var/www/html/site/config.secrets.php
set_processwire_option() {
  # shellcheck disable=SC2016
  printf '$config->%s = '"'%s'"';\n' "$1" "$2" >> /var/www/html/site/config.secrets.php
}

set_processwire_option dbHost "${MARIADB_HOST:-}"; unset MARIADB_HOST
set_processwire_option dbPort "${MARIADB_PORT:-3306}"; unset MARIADB_PORT
set_processwire_option dbName "${MARIADB_DATABASE:-}"; unset MARIADB_DATABASE
set_processwire_option dbUser "${MARIADB_USERNAME:-}"; unset MARIADB_USERNAME
set_processwire_option dbPass "${MARIADB_PASSWORD:-}"; unset MARIADB_PASSWORD
set_processwire_option httpHosts "[${DOMAIN:-}]"; unset DOMAIN
